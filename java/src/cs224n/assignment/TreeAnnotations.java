package cs224n.assignment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cs224n.ling.Tree;
import cs224n.ling.Trees;
import cs224n.util.Filter;

/**
 * Class which contains code for annotating and binarizing trees for
 * the parser's use, and debinarizing and unannotating them for
 * scoring.
 */
public class TreeAnnotations {

  private static int order = 0;

	public static Tree<String> annotateTree(Tree<String> unAnnotatedTree) {

		// Currently, the only annotation done is a lossless binarization

		// TODO: change the annotation from a lossless binarization to a
		// finite-order markov process (try at least 1st and 2nd order)

		// TODO : mark nodes with the label of their parent nodes, giving a second
		// order vertical markov process

    Tree<String> markovTree = markovizeTree(unAnnotatedTree);
    //System.out.println(markovTree);
		Tree<String> bTree = binarizeTree(markovTree);
    // System.out.println(bTree);
    return bTree;

	}

  private static Tree<String> markovizeTree(Tree<String> tree) {
    if (order == 0) return tree;
    if (order == 1)
      return markovizeTree1(tree, "");
    return markovizeTree2(tree, "","");
  }

  private static Tree<String> demarkovizeTree(Tree<String> tree) {
    if (order == 0) return tree;
    if (order == 1)
      return demarkovizeTree1(tree);
    return demarkovizeTree2(tree);
  }

  private static Tree<String> markovizeTree1(Tree<String> tree, String parent) {
    // String markovLabel = parent + ":" + tree.getLabel();
    // System.out.println(markovLabel);
    if (tree.isLeaf())
      return new Tree<String>(tree.getLabel());

    String markovLabel = parent + ":" + tree.getLabel();
    Tree<String> markovTree = new Tree<String>(markovLabel);
    List<Tree<String>> children = new ArrayList<Tree<String>>();
    for (Tree<String> child : tree.getChildren()) {
      children.add(markovizeTree1(child, tree.getLabel()));
    }
    markovTree.setChildren(children);
    // System.out.println(markovTree);
    return markovTree;
  }

  private static Tree<String> demarkovizeTree1(Tree<String> tree) {
    String markovLabel = tree.getLabel();
    // System.out.println("markovLabel:  " + markovLabel);
    int ind = markovLabel.indexOf(':');
    String label = (ind != -1) ? markovLabel.substring(ind+1) : markovLabel;
    // System.out.println("label:  " + label);
    if (tree.isLeaf()) {
      // System.out.println("leaf: " + label);
      return new Tree<String>(label);
    }
    Tree<String> demarkovTree = new Tree<String>(label);
    List<Tree<String>> children = new ArrayList<Tree<String>>();
    for (Tree<String> child : tree.getChildren()) {
      children.add(demarkovizeTree1(child));
    }
    demarkovTree.setChildren(children);
    return demarkovTree;
  }

  private static Tree<String> markovizeTree2(Tree<String> tree, String grandparent, String parent) {
    // String markovLabel = grandparent + ":" + parent + ":" + tree.getLabel();
    if (tree.isLeaf())
      return new Tree<String>(tree.getLabel());

    String markovLabel = grandparent + ":" + parent + ":" + tree.getLabel();
    Tree<String> markovTree = new Tree<String>(markovLabel);
    List<Tree<String>> children = new ArrayList<Tree<String>>();
    for (Tree<String> child : tree.getChildren()) {
      children.add(markovizeTree2(child, parent, tree.getLabel()));
    }
    markovTree.setChildren(children);
    return markovTree;
  }

  private static Tree<String> demarkovizeTree2(Tree<String> tree) {
    String markovLabel = tree.getLabel();
    int ind = markovLabel.indexOf(':');
    String label = (ind != -1) ? markovLabel.substring(ind+1) : markovLabel;
    ind = label.indexOf(':');
    label = (ind != -1) ? label.substring(ind+1) : label;

    if (tree.isLeaf()) {
      return new Tree<String>(label);
    }
    Tree<String> demarkovTree = new Tree<String>(label);
    List<Tree<String>> children = new ArrayList<Tree<String>>();
    for (Tree<String> child : tree.getChildren()) {
      children.add(demarkovizeTree2(child));
    }
    demarkovTree.setChildren(children);
    return demarkovTree;
  }



	private static Tree<String> binarizeTree(Tree<String> tree) {
		String label = tree.getLabel();
		if (tree.isLeaf())
			return new Tree<String>(label);
		if (tree.getChildren().size() == 1) {
			return new Tree<String>
			(label, 
					Collections.singletonList(binarizeTree(tree.getChildren().get(0))));
		}
		// otherwise, it's a binary-or-more local tree, 
		// so decompose it into a sequence of binary and unary trees.
		String intermediateLabel = "@"+label+"->";
		Tree<String> intermediateTree =
				binarizeTreeHelper(tree, 0, intermediateLabel);
		return new Tree<String>(label, intermediateTree.getChildren());
	}

	private static Tree<String> binarizeTreeHelper(Tree<String> tree,
			int numChildrenGenerated, 
			String intermediateLabel) {
		Tree<String> leftTree = tree.getChildren().get(numChildrenGenerated);
		List<Tree<String>> children = new ArrayList<Tree<String>>();
		children.add(binarizeTree(leftTree));
		if (numChildrenGenerated < tree.getChildren().size() - 1) {
			Tree<String> rightTree = 
					binarizeTreeHelper(tree, numChildrenGenerated + 1, 
							intermediateLabel + "_" + leftTree.getLabel());
			children.add(rightTree);
		}
		return new Tree<String>(intermediateLabel, children);
	} 

	public static Tree<String> unAnnotateTree(Tree<String> annotatedTree) {

		// Remove intermediate nodes (labels beginning with "@"
		// Remove all material on node labels which follow their base symbol 
		// (cuts at the leftmost -, ^, or : character)
		// Examples: a node with label @NP->DT_JJ will be spliced out, 
		// and a node with label NP^S will be reduced to NP

		Tree<String> debinarizedTree =
				Trees.spliceNodes(annotatedTree, new Filter<String>() {
					public boolean accept(String s) {
						return s.startsWith("@");
					}
				});
    // System.out.println("\n\n"+ debinarizedTree);
    Tree<String> demarkovTree = demarkovizeTree(debinarizedTree);
		Tree<String> unAnnotatedTree =
				(new Trees.FunctionNodeStripper()).transformTree(demarkovTree);
		return unAnnotatedTree;
	}
}
