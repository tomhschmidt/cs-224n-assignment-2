package cs224n.assignment;

import cs224n.ling.Tree;
import cs224n.ling.Constituent;
import cs224n.util.*;
import java.util.*;
import cs224n.assignment.Grammar.UnaryRule;

/**
 * The CKY PCFG Parser you will implement.
 */
public class PCFGParser implements Parser {
    private Grammar grammar;
    private Lexicon lexicon;
    //private Map<String,Integer> nonterms;

    public void train(List<Tree<String>> trainTrees) {
        // TODO: before you generate your grammar, the training trees
        // need to be binarized so that rules are at most binary
        List<Tree<String>> binarizedTrees = new ArrayList<Tree<String>>();
        for (Tree<String> tree : trainTrees)
            binarizedTrees.add(TreeAnnotations.annotateTree(tree));

        lexicon = new Lexicon(binarizedTrees);
        grammar = new Grammar(binarizedTrees);
    }

    public Tree<String> getBestParse(List<String> sentence) {

        ArrayList<ArrayList<HashMap<String, Double>>> scores = new ArrayList<ArrayList<HashMap<String, Double>>>();
        for(int i = 0; i < sentence.size() + 1; i++) {
          scores.add(i, new ArrayList<HashMap<String, Double>>());
          for(int j = 0; j < sentence.size() + 1; j++) {
            scores.get(i).add(j, new HashMap<String, Double>());
          }
        }

        Triplet<Integer, Integer, String> root;

        ArrayList<ArrayList<HashMap<String, Triplet<Integer, String, String>>>> backtraces = new ArrayList<ArrayList<HashMap<String, Triplet<Integer, String, String>>>>();

        for(int i = 0; i < sentence.size() + 1; i++) {
          backtraces.add(i, new ArrayList<HashMap<String, Triplet<Integer, String, String>>>());
          for(int j = 0; j < sentence.size() + 1; j++) {
            backtraces.get(i).add(j, new HashMap<String, Triplet<Integer, String, String>>());
          }
        }

        // Handle scores of words
        Set<String> nonterms = new HashSet(lexicon.getAllTags());
        Set<Triplet<Integer, Integer, String>> replacedLocations = new HashSet<Triplet<Integer, Integer, String>>();
        nonterms.addAll(grammar.allNonTerms);

        Map<String, List<UnaryRule>>  unaryRulesByChild = new HashMap<String, List<UnaryRule>>();
        for (int i = 0; i < sentence.size(); i++) {
          for (String tag : nonterms) {
            double score = lexicon.scoreTagging(sentence.get(i), tag);
            if (score > 0) {
              scores.get(i).get(i+1).put(tag, score);
            }
          }

          // Handle Unary transitions
          // System.out.println("=======Checking Unaries========");
          boolean added = true;
          while(added) {
            added = false;
            Set<String> unseenNonterms = new HashSet<String>();
            for (String B: nonterms) {
              Triplet current = new Triplet(i,i+1,B);

              if (!scores.get(i).get(i+1).containsKey(B)) {
                continue;
              }

              double currentScore = scores.get(i).get(i+1).get(B);
              if (currentScore > 0 && !replacedLocations.contains(current)) {

                List<UnaryRule> unaryRules;
                if(!unaryRulesByChild.containsKey(B)) {
                  unaryRulesByChild.put(B, grammar.getUnaryRulesByChild(B));
                }

                unaryRules = unaryRulesByChild.get(B);

                for (Grammar.UnaryRule rule : unaryRules) {
                  String A = rule.getParent();
                  unseenNonterms.add(A);

                  double probA = rule.getScore()*currentScore;
                  Triplet next = new Triplet(i,i+1,A);

                  if (!scores.get(i).get(i+1).containsKey(A)) {
                    scores.get(i).get(i+1).put(A,0.0);
                  }

                  if (probA > scores.get(i).get(i+1).get(A)) {
                    scores.get(i).get(i+1).put(A, probA);
                    backtraces.get(i).get(i+1).put(A, new Triplet(-1, B, null));
                    added = true;
                  }
                }
              }
            }
          }
        }

        // Build up Parse
        for (int span = 2; span <= sentence.size(); span++) {
          for (int begin = 0; begin <= sentence.size()-span; begin++) {
            int end = begin + span;
            // System.out.println("=======Checking Binaries========");
            for (int split = begin+1; split < end; split++) {
              // Set<String> unseenNonterms = new HashSet<String>();
              for (String B : grammar.allNonTerms) {
                for (Grammar.BinaryRule rule : grammar.getBinaryRulesByLeftChild(B)) {
                  String A = rule.getParent();
                  // unseenNonterms.add(A);
                  String C = rule.getRightChild();

                  Triplet front = new Triplet(begin,split,B);
                  Triplet back = new Triplet(split, end, C);
                  Triplet all = new Triplet(begin, end, A);

                  //if(A.equals("ROOT")) {
                    // System.out.println("Hitting root!");
                  //}
                  if (!scores.get(begin).get(split).containsKey(B)) {
                    continue;
                    //scores.put(front, 0.0);
                  }
                  if (!scores.get(split).get(end).containsKey(C)) {
                    continue;
                    //scores.put(back, 0.0);
                  }
                  if (!scores.get(begin).get(end).containsKey(A)) {
                    scores.get(begin).get(end).put(A, 0.0);
                  }

                  double prob = scores.get(begin).get(split).get(B) * scores.get(split).get(end).get(C) * rule.getScore();
                  if (prob > scores.get(begin).get(end).get(A)) {
                    scores.get(begin).get(end).put(A, prob);
                    backtraces.get(begin).get(end).put(A, new Triplet(split,B,C));
                  }
                }
              }
              //nonterms.addAll(unseenNonterms);
            }

            // System.out.println("=======Checking Unaries========");

            // Handle Unaries
            boolean added = true;
            while (added) {
              added = false;
              for (String B : nonterms) {

                Triplet currentB = new Triplet(begin,end,B);

                if(!scores.get(begin).get(end).containsKey(B)) {
                  continue;
                }

                double currentBScore = scores.get(begin).get(end).get(B);

                if(currentBScore > 0 && !replacedLocations.contains(currentB)) {
                    
                    List<UnaryRule> unaryRules;
                    if(!unaryRulesByChild.containsKey(B)) {
                      unaryRulesByChild.put(B, grammar.getUnaryRulesByChild(B));
                    }

                    unaryRules = unaryRulesByChild.get(B);
                    for (Grammar.UnaryRule rule : unaryRules) {
                      String A = rule.getParent();

                      Triplet currentA = new Triplet(begin,end,A);

                      if (!scores.get(begin).get(end).containsKey(A)) {
                        scores.get(begin).get(end).put(A,0.0);
                      }

                      double probA = rule.getScore()*currentBScore;
                      if (probA > scores.get(begin).get(end).get(A)) {
                        scores.get(begin).get(end).put(A, probA);
                        backtraces.get(begin).get(end).put(A, new Triplet(-1, B, null));
                        replacedLocations.add(currentB);
                        added = true;
                      }
                    }
                  }
              }
          }
        }
      }

      Tree<String> finalTree = buildTree(new Triplet(0, sentence.size(), "ROOT"), backtraces, sentence);
      Tree<String> unbinarizedTree = TreeAnnotations.unAnnotateTree(finalTree);

      return unbinarizedTree;
    }

    private Tree<String> buildTree(Triplet<Integer, Integer, String> root, ArrayList<ArrayList<HashMap<String, Triplet<Integer, String, String>>>> backtraces, List<String> sentence) {
        Triplet<Integer, String, String> backtrace = backtraces.get(root.getFirst()).get(root.getSecond()).get(root.getThird());

        List<Tree<String>> children = new ArrayList<Tree<String>>();

        if(backtrace != null) {

          if(backtrace.getFirst() == -1) {
            Tree<String> unaryChild = buildTree(new Triplet(root.getFirst(), root.getSecond(), backtrace.getSecond()), backtraces, sentence);
            children.add(unaryChild);
          }
          else {

            Triplet<Integer, Integer, String> leftChildTriplet = new Triplet(root.getFirst(), backtrace.getFirst(), backtrace.getSecond());
            Triplet<Integer, Integer, String> rightChildTriplet = new Triplet(backtrace.getFirst(), root.getSecond(), backtrace.getThird());

            Tree<String> leftChild = buildTree(leftChildTriplet, backtraces, sentence);
            Tree<String> rightChild = buildTree(rightChildTriplet, backtraces, sentence);

            children.add(leftChild);
            children.add(rightChild);
          }


          Tree<String> curRoot = new Tree<String>(root.getThird(), children);

          return curRoot;
        }

        ArrayList<Tree<String>> word = new ArrayList<Tree<String>>();
        word.add(new Tree<String>(sentence.get(root.getFirst()), new ArrayList<Tree<String>>()));
        return new Tree<String>(root.getThird(), word);

    }
}
